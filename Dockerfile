FROM maven:3-jdk-8-alpine

EXPOSE 8080
ADD target/demo-0.0.1-SNAPSHOT.jar spring-app.jar
ENTRYPOINT ["java", "-jar", "spring-app.jar"]
